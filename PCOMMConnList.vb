﻿Public Class PCOMMConnList

    Private connectionList As Object
    Private connections As Dictionary(Of String, PCOMMConn)

    Public Sub New()
        connectionList = CreateObject("PCOMM.autECLConnList")
        connections = New Dictionary(Of String, PCOMMConn)
    End Sub

    Friend Sub New(conListObj As Object)
        connectionList = conListObj
        connections = New Dictionary(Of String, PCOMMConn)
    End Sub


    Public ReadOnly Property Count() As Integer
        Get
            Return connectionList.Count
        End Get
    End Property

    ''' <summary>
    ''' Get Acive connection list from underlying dictionary order alphabetically
    ''' </summary>
    ''' <returns><c>IEnumerable<PCOMMConn></PCOMMConn></c></returns>
    Public Function GetActiveConnections() As IEnumerable(Of PCOMMConn)
        Return connections.Values.Where(Function(z) z.State = PCOMMConnectionState.ACTIVE).OrderBy(Function(x) x.Name)
    End Function


    ''' <summary>
    ''' Refreash connection list and underlying dictionary
    ''' </summary>
    Public Sub Refresh()
        connectionList.Refresh()
        Dim tempConnections As New Dictionary(Of String, PCOMMConn)(connections)
        For Each con In tempConnections
            If con.Value.State <> PCOMMConnectionState.BUSY Then
                connections(con.Key) = GetConnection(con.Key)
            End If
        Next
    End Sub


    ''' <summary>
    ''' Look for connection by name. Returns PCOMMConn object with .State ACTIVE if connection was found, if not Returns PCOMMConn object with .State INACTIVE. Omit BUSY connections
    ''' </summary>
    ''' <param name="name">connection name [A-Z] capital</param>
    ''' <returns></returns>
    Public Function FindConnectionByName(name As String) As PCOMMConn

        Dim connection As PCOMMConn

        If connections.ContainsKey(name) Then
            If connections(name).State <> PCOMMConnectionState.BUSY Then
                connections(name) = GetConnection(name)
            End If

            Return connections(name)
        Else
            connection = GetConnection(name)
            connections.Add(connection.Name, connection)
            Return connection
        End If

    End Function

    Private Function GetConnection(name As String) As PCOMMConn

        Dim con = connectionList.FindConnectionByName(name)

        If Not IsNothing(con) Then
            Return New PCOMMConn(con)
        End If

        Return New PCOMMConn(name)

    End Function

End Class
