﻿Public Class PCOMMOIA

    Private operationInformation As Object

    Friend Sub New(oiaSessionObj As Object)
        operationInformation = oiaSessionObj
    End Sub

    Public Sub WaitForInputReady()
        If operationInformation.WaitForInputReady() Then
        End If
    End Sub

    Public Sub WaitForAppAvailable()
        If operationInformation.WaitForAppAvailable() Then
        End If
    End Sub


    ''' <summary>
    ''' Cool stuff, If You changing window call this to wait, after sending mnemonic key, errors will happen if You don't
    ''' </summary>
    Public Sub WaitForPCOMM()
        If operationInformation.WaitForAppAvailable() Then
        End If
        If operationInformation.WaitForInputReady() Then
        End If
    End Sub

End Class
