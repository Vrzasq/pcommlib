﻿''' <summary>
''' PCOMMConnManager manages all Personal Communications connections on a given machine.
''' It contains methods relating to the connection management such as starting and stopping connections.
''' It also creates an PCOMMConnList
''' </summary>
Public Class PCOMMConnManager

    Private manager As Object
    Private conList As PCOMMConnList

    Public Sub New()
        manager = CreateObject("PCOMM.autECLConnMgr")
        conList = New PCOMMConnList(manager.autECLConnList)
    End Sub

    ''' <summary>
    ''' PCOMMConnList object
    ''' </summary>
    ''' <returns>PCOMMConnList</returns>
    Public ReadOnly Property ConnList() As PCOMMConnList
        Get
            Return conList
        End Get
    End Property

    ''' <summary>
    ''' This member function starts a new Personal Communications emulator window. The conParams string contains connection configuration information.
    ''' </summary>
    ''' <example>
    '''     <code>
    '''     obj.StartConnection("PROFILE=c:\\file.ws CONNAME=A")
    '''     </code>
    ''' </example>
    ''' <param name="conParams">
    ''' PROFILE=[']filename['] [CONNNAME=c] [WINSTATE=MAX|MIN|RESTORE|HIDE]
    ''' <para>PROFILE=[']filename[']: Names the Personal Communications workstation profile (.WS file), which contains the configuration information. This parameter is not optional; a profile name must be supplied. If the file name contains blanks the name must be enclosed in single quotation marks.</para>
    ''' <para>CONNNAME=c: Specifies the short ID of the new connection. This value must be a single, alphabetic character (A-Z or a-z). If this value is not specified, the next available connection ID is assigned automatically.</para>
    ''' <para>WINSTATE=MAX|MIN|RESTORE|HIDE: Specifies the initial state of the emulator window. The default if this parameter is not specified is RESTORE.</para>
    ''' </param>
    Public Sub StartConnection(conParams As String)
        manager.StartConnection(conParams)
    End Sub



    ''' <summary>
    ''' The StopConnection method stops (terminates) the emulator window identified by the connection handle.
    ''' example: obj.StopConnection("A", "SAVEPROFILE=NO")
    ''' </summary>
    ''' <param name="connectionName">Connection name [A-Z]</param>
    ''' <param name="stopParams">SAVEPROFILE=YES|NO|DEFAULT</param>
    Public Sub StopConnection(connectionName As String, Optional stopParams As String = "")
        If stopParams <> "" Then
            manager.StopConnection(connectionName, stopParams)
        Else
            manager.StopConnection(connectionName)
        End If
    End Sub

End Class
