﻿Public Enum PCOMMConnectionState
    INACTIVE
    ACTIVE
    BUSY
End Enum

''' <summary>
''' Member of PCOMMConnList
''' </summary>
Public Class PCOMMConn

    Private _name As String
    Private _handle As Integer
    Private _ready As Boolean
    Private _state As PCOMMConnectionState

    Private connection As Object

    Friend Sub New(conName As String)
        _name = conName
        _ready = False
        _state = PCOMMConnectionState.INACTIVE
    End Sub

    Friend Sub New(conListObj As Object)
        connection = conListObj
        SetupConnection()
    End Sub

    Public ReadOnly Property State() As PCOMMConnectionState
        Get
            Return _state
        End Get
    End Property

    Public ReadOnly Property Name() As String
        Get
            Return _name
        End Get
    End Property

    Public ReadOnly Property Handle() As Integer
        Get
            Return _handle
        End Get
    End Property


    Public ReadOnly Property Ready() As Boolean
        Get
            Return _ready
        End Get
    End Property

    Public Sub ChangeState(state As PCOMMConnectionState)
        _state = state
    End Sub

    Public Sub StartCommunication()
        connection.StartCommunication()
    End Sub

    Public Sub StopCommunication()
        connection.StopCommunication()
    End Sub

    Private Sub SetupConnection()
        If Not IsNothing(connection) Then
            _ready = connection.Ready
            _name = connection.Name
            _handle = connection.Handle
            If _ready Then
                _state = PCOMMConnectionState.ACTIVE
            End If
        End If
    End Sub
End Class
