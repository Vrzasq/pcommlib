﻿Public Class PCOMMPSpace

    Private presenationSpace As Object
    Private fl As PCOMMFieldList

    Friend Sub New(psObj As Object)
        presenationSpace = psObj
        fl = New PCOMMFieldList(presenationSpace.autECLFieldList)
    End Sub

    Public ReadOnly Property FieldList() As PCOMMFieldList
        Get
            Return fl
        End Get
    End Property

    Public ReadOnly Property CursorPosRow() As Integer
        Get
            Return presenationSpace.CursorPosRow
        End Get
    End Property

    Public ReadOnly Property CursorPosCol() As Integer
        Get
            Return presenationSpace.CursorPosCol
        End Get

    End Property

    ''' <summary>
    ''' Get current cursor position in emulator window
    ''' </summary>
    ''' <returns>CursorPosition object</returns>
    Public Function GetCursorPosition() As CursorPosition
        Return New CursorPosition(
            presenationSpace.CursorPosRow,
            presenationSpace.CursorPosCol)
    End Function

    Public Sub SetCursorPos(row As Integer, col As Integer)
        presenationSpace.SetCursorPos(row, col)
    End Sub

    Public Sub SetCursorPos(cPosition As CursorPosition)
        presenationSpace.SetCursorPos(cPosition.Row, cPosition.Col)
    End Sub


    ''' <summary>
    ''' Usefull for sending mnemonic keys to emulator window, can be send to a specyfic position
    ''' </summary>
    ''' <param name="key">Mnemonic key stored in PCOMMButtons object</param>
    ''' <param name="row"></param>
    ''' <param name="col"></param>
    Public Sub SendKeys(key As String, Optional row As Integer = 0, Optional col As Integer = 0)
        If row = 0 And col = 0 Then
            presenationSpace.SendKeys(key)
        Else
            presenationSpace.SendKeys(key, row, col)
        End If
    End Sub


    ''' <summary>
    ''' Clear field in emulator window under row, col position
    ''' </summary>
    ''' <param name="row"></param>
    ''' <param name="col"></param>
    Public Sub ClearField(row As Integer, col As Integer)
        presenationSpace.SendKeys(PCOMMButtons.NUM_PLUS, row, col)
    End Sub

    ''' <summary>
    ''' Search spcyfied string in emulator window, returns True if string was found, False otherwise.
    ''' </summary>
    ''' <param name="text">string to search</param>
    ''' <param name="dir">direction 1 - forward, 2 - backward</param>
    ''' <param name="row">start row</param>
    ''' <param name="col">start col</param>
    ''' <returns></returns>
    Public Function SearchText(text As String, Optional dir As Integer = 1, Optional row As Integer = 0, Optional col As Integer = 0) As Boolean
        If row = 0 And col = 0 Then
            Return presenationSpace.SearchText(text, dir)
        Else
            Return presenationSpace.SearchText(text, dir, row, col)
        End If
    End Function

    Public Sub SetText(text As String, row As Integer, col As Integer)
        presenationSpace.SetText(text, row, col)
    End Sub

    Public Function GetTextRect(startRow As Integer, startCol As Integer, endRow As Integer, endCol As Integer) As String
        Return presenationSpace.GetTextRect(startRow, startCol, endRow, endCol)
    End Function

    ''' <summary>
    ''' Sleep / Wait
    ''' </summary>
    ''' <param name="milliseconds">time to wait in milliseconds, 1000ms = 1s</param>
    Public Sub Wait(milliseconds As Integer)
        presenationSpace.Wait(milliseconds)
    End Sub

    Public Sub CancelWaits()
        presenationSpace.CancelWaits()
    End Sub

    ''' <summary>
    ''' Wait until emulator's cursor get in position row, col
    ''' </summary>
    ''' <param name="row"></param>
    ''' <param name="col"></param>
    ''' <param name="timeOut"></param>
    ''' <param name="bWaitForIr"></param>
    ''' <returns></returns>
    Public Function WaitForCursor(row As Integer, col As Integer, Optional timeOut As Integer = 0, Optional bWaitForIr As Boolean = False) As Boolean
        If timeOut = 0 And Not bWaitForIr Then
            Return presenationSpace.WaitForCursor(row, col)
        End If
        If timeOut <> 0 And Not bWaitForIr Then
            Return presenationSpace.WaitForCursor(row, col, timeOut)
        End If
        Return presenationSpace.WaitForCursor(row, col, timeOut, bWaitForIr)
    End Function

    ''' <summary>
    ''' Wait while cursor is set in row, col position
    ''' </summary>
    ''' <param name="row"></param>
    ''' <param name="col"></param>
    ''' <param name="timeOut"></param>
    ''' <param name="bWaitForIr"></param>
    ''' <returns></returns>
    Public Function WaitWhileCursor(row As Integer, col As Integer, Optional timeOut As Integer = 0, Optional bWaitForIr As Boolean = False) As Boolean
        If timeOut = 0 And Not bWaitForIr Then
            Return presenationSpace.WaitWhileCursor(row, col)
        End If
        If timeOut <> 0 And Not bWaitForIr Then
            Return presenationSpace.WaitWhileCursor(row, col, timeOut)
        End If
        Return presenationSpace.WaitWhileCursor(row, col, timeOut, bWaitForIr)
    End Function


    ''' <summary>
    ''' Wait unstil specified stirng appears in position row, col
    ''' </summary>
    ''' <param name="waitString">string to wait for</param>
    ''' <param name="row"></param>
    ''' <param name="col"></param>
    ''' <param name="timeOut"></param>
    ''' <param name="bWaitForIr"></param>
    ''' <param name="bCaseSens"></param>
    ''' <returns></returns>
    Public Function WaitForString(waitString As String, Optional row As Integer = 0, Optional col As Integer = 0, Optional timeOut As Integer = 0, Optional bWaitForIr As Boolean = False, Optional bCaseSens As Boolean = False) As Boolean
        Return presenationSpace.WaitForString(waitString, row, col, timeOut, bWaitForIr, bCaseSens)
    End Function

    ''' <summary>
    ''' Wait while spcified string is displayed in position row, col
    ''' </summary>
    ''' <param name="waitString">visible string</param>
    ''' <param name="row"></param>
    ''' <param name="col"></param>
    ''' <param name="timeOut"></param>
    ''' <param name="bWaitForIr"></param>
    ''' <param name="bCaseSens"></param>
    ''' <returns></returns>
    Public Function WaitWhileString(waitString As String, Optional row As Integer = 0, Optional col As Integer = 0, Optional timeOut As Integer = 0, Optional bWaitForIr As Boolean = False, Optional bCaseSens As Boolean = False) As Boolean
        Return presenationSpace.WaitWhileString(waitString, row, col, timeOut, bWaitForIr, bCaseSens)
    End Function

    Public Function GetText(row As Integer, col As Integer, length As Integer) As String
        Return Trim(presenationSpace.GetText(row, col, length))
    End Function

End Class