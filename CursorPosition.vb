﻿''' <summary>
''' Represent PCOMM cursor poistion in emulator window.
''' </summary>
Public Class CursorPosition

    Public Sub New(r As Integer, c As Integer)
        Row = r
        Col = c
    End Sub

    Private _row As Integer
    Public Property Row() As Integer
        Get
            Return _row
        End Get
        Set(ByVal value As Integer)
            _row = value
        End Set
    End Property

    Private _col As Integer
    Public Property Col() As Integer
        Get
            Return _col
        End Get
        Set(ByVal value As Integer)
            _col = value
        End Set
    End Property

    ''' <summary>
    ''' Return "row col" string
    ''' </summary>
    ''' <returns></returns>
    Public Overrides Function ToString() As String
        Return (_row.ToString() & " " & _col.ToString())
    End Function

End Class
