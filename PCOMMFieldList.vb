﻿Public Class PCOMMFieldList

    Private fieldList As Object

    Friend Sub New(fieldListPSObj As Object)
        fieldList = fieldListPSObj
    End Sub

    Public ReadOnly Property Count() As Integer
        Get
            Return fieldList.Count
        End Get
    End Property


    ''' <summary>
    ''' Get Trimed string under row, col in emulator window. Call Refresh() before using
    ''' </summary>
    ''' <param name="row"></param>
    ''' <param name="col"></param>
    ''' <returns>feild string</returns>
    Public Function GetFieldData(row As Integer, col As Integer) As String
        Return Trim(fieldList.FindFieldByRowCol(row, col).GetText())
    End Function


    ''' <summary>
    ''' Refresh snapshot of stored fieldlist in emulator window. Call it before GetFieldData()
    ''' </summary>
    Public Sub Refresh()
        fieldList.Refresh()
    End Sub

End Class
