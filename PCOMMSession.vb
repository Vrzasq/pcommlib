﻿Public Class PCOMMSession

    Private session As Object
    Private space As PCOMMPSpace
    Private operationInformation As PCOMMOIA

    Public Sub New()
        session = CreateObject("PCOMM.autECLSession")
    End Sub


    ''' <summary>
    ''' Includes SetConnectionByName method
    ''' </summary>
    ''' <param name="connectionName">Connection Name</param>
    Public Sub New(connectionName As String)
        session = CreateObject("PCOMM.autECLSession")
        SetConnectionByName(connectionName)
    End Sub

    ''' <summary>
    ''' Includes SetConnectionByHandle method
    ''' </summary>
    ''' <param name="connectionHandle"></param>
    Public Sub New(connectionHandle As Integer)
        session = CreateObject("PCOMM.autECLSession")
        SetConnectionByHandle(connectionHandle)
    End Sub


    ''' <summary>
    ''' Must be called after creation, unless object was created with parametred constructor than it will couse and error
    ''' </summary>
    ''' <param name="name">Connection Name</param>
    Public Sub SetConnectionByName(name As String)
        session.SetConnectionByName(name)
        space = New PCOMMPSpace(session.autECLPS)
        operationInformation = New PCOMMOIA(session.autECLOIA)
    End Sub

    ''' <summary>
    ''' Must be called after creation, unless object was created with parametred constructor than it will couse and error
    ''' </summary>
    ''' <param name="handle">Connection Handle</param>
    Public Sub SetConnectionByHandle(handle As Integer)
        session.SetConnectionByName(Name)
        space = New PCOMMPSpace(session.autECLPS)
        operationInformation = New PCOMMOIA(session.autECLOIA)
    End Sub

    Public ReadOnly Property Name() As String
        Get
            Return session.Name
        End Get
    End Property

    Public ReadOnly Property Handle() As Integer
        Get
            Return session.Handle
        End Get
    End Property

    Public ReadOnly Property Ready() As Boolean
        Get
            Return session.Ready
        End Get
    End Property

    Public ReadOnly Property PSpace() As PCOMMPSpace
        Get
            Return space
        End Get
    End Property

    Public ReadOnly Property OIA() As PCOMMOIA
        Get
            Return operationInformation
        End Get
    End Property

End Class
